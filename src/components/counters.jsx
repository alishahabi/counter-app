import React, { Component } from "react";
import Counter from "./counter";

class Counters extends Component {
  render() {
    // console.log("counters renderd ");
    const { onReset, onDelete, onIncrement,onDecrement,onLike, counters } = this.props;
    return (
      <div>
        <button onClick={onReset} className="btn btn-primary  m-2">
          Reset
        </button>
        {counters.map((counter) => (
          <Counter
            key={counter.id}
            onDecrement={onDecrement}
            onDelete={onDelete}
            onIncrement={onIncrement}
            onLike={onLike}
            counter={counter}
          />
        ))}
      </div>
    );
  }
}

export default Counters;
