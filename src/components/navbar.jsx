const NavBar = ({ totalCounter }) => {
  // console.log("Navbar renderd")
  return (
    <nav className="navbar navbar-light bg-light">
      <div className="container-fluid">
        <a className="navbar-brand" href="http://localhost:3000/">
          Navbar{" "}
          <span className="badge badge-pill badge-secondary">
            {totalCounter}
          </span>
        </a>
      </div>
    </nav>
  );
};

export default NavBar;
